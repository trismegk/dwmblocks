//Modify this file to change what commands output to your statusbar, and recompile using the make command.
#define PATH "/home/vincent/.local/src/dwmblocks/dwmblocks-scripts/"
#define GET_PATH(x) PATH x
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	/* {"⌨", "sb-kbselect", 0, 30}, */
//	{"", "cat /tmp/recordingicon 2>/dev/null",	0,	9},
//	{"",	"sb-tasks",	10,	26},
//	{"",	"sb-music",	0,	11},
//	{"",	"sb-pacpackages",	0,	8},
//	{"",	"sb-news",		0,	6},
	/* {"",	"sb-price lbc \"LBRY Token\" 📚",			9000,	22}, */
	/* {"",	"sb-price bat \"Basic Attention Token\" 🦁",	9000,	20}, */
	/* {"",	"sb-price link \"Chainlink\" 🔗",			300,	25}, */
	{" XMR: ",	GET_PATH("sb-price xmr \"Monero\" 🔒"),			9000,	24},
	/* {"",	"sb-price eth Ethereum 🍸",	9000,	23}, */
	{"BTC: ",	GET_PATH("sb-price btc Bitcoin 💰"),				9000,	21},
//	{"",	"sb-torrent",	20,	7},
	{"RAM: ",	GET_PATH("sb-memory"),	10,	14},
//	{"CPU",	        GET_PATH("sb-cpu"),		10,	18},
	/* {"",	"sb-moonphase",	18000,	17}, */
//	{	"",	GET_PATH("sb-forecast"),	18000,	5},
//	{"",	"sb-mailbox",	180,	12},
	{"NET: ",	GET_PATH("sb-nettraf"),	1,	16},
//	{"",	"sb-volume",	0,	10},
	{"CHARGE: ",	GET_PATH("sb-battery"),	5,	10},
//	{"",	"sb-clock",	60,	1},
// 	{"NET ",	"sb-internet",	5,	4},
	{"PORNFOLDER: ",GET_PATH("sb-disk"),	5,	4},
//	"UPT: ",	"sb-upt",	5,	10,
	{"KRNL: ",	GET_PATH("sb-kernel"),	0,	4},
	//	{"",	"sb-help-icon",	0,	15},
};

//Sets delimiter between status commands. NULL character ('\0') means no delimiter.
static char *delim = " | ";

// Have dwmblocks automatically recompile and run when you edit this file in
// vim with the following line in your vimrc/init.vim:

// autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }

